### presentation_remote

This is an android app used to controll presentations made with my other project.
It's made using libsoup and my kiss-nanovg-sdl library.

### screenshot

![app screenshot](screenshot.png "app screenshot")

[Hacker's keyboard](https://github.com/klausw/hackerskeyboard) not included

### usage

You just enter url of your presentation in the top entry, and your password in the bottom one. Then you click OK, open your presentation on the presenting device, and click on the screen every time you wish to go one step further in the presentation. in case you need to revert, you can use the back button.

### building

Everything you need is already in here. A script to build the "libsoup-fat.so" library was added, but is untested because of low bandwitdth
