GCC=arm-linux-androideabi-gcc
gstreamer_arch="armv7"

gstreamer_ver="1.12.3"
gstreamer_filename="gstreamer-1.0-android-universal-${gstreamer_ver}.tar.bz2"
gstreamer_url="http://gstreamer.freedesktop.org/data/pkg/android/${gstreamer_ver}/${gstreamer_filename}"

mkdir tmp
cd tmp

echo "downloading gstreamer's prebuilt libraries ..."
wget ${gstreamer_url}
echo "extracting ..."
tar -xjf ${gstreamer_filename}

cd ${gstreamer_arch}

echo "fixing libgnutls's duplicate xsize.o ..."
cp lib/libgnutls.a lib/libgnutls_fix.a
ar d lib/libgnutls_fix.a xsize.o

echo "compiling ..."
${GCC} -shared -o libsoup-fat.so -Wl,-soname,libsoup-fat.so -Wl,--whole-archive lib/gio/modules/static/libgiognutls.a lib/libsoup-2.4.a lib/libgnutls_fix.a lib/libhogweed.a lib/libtasn1.a lib/libgmp.a lib/libnettle.a lib/libxml2.a lib/libgio-2.0.a lib/libgobject-2.0.a lib/libgmodule-2.0.a lib/libglib-2.0.a lib/libz.a lib/libiconv.a lib/libintl.a lib/libffi.a -Wl,--no-whole-archive -lm

cp libsoup-fat.so app/jni/libsoup-fat/

echo "app/jni/libsoup-fat/libsoup-fat.so should now exist"
