#include "SDL.h"

char* file_read(const char *filename) {
        SDL_RWops *rw = SDL_RWFromFile(filename, "rb");
        if (rw == NULL) return NULL;

        Sint64 res_size = SDL_RWsize(rw);
        char* res = (char*)malloc(res_size + 1);

        Sint64 nb_read_total = 0, nb_read = 1;
        char* buf = res;
        while (nb_read_total < res_size && nb_read != 0) {
                nb_read = SDL_RWread(rw, buf, 1, (res_size - nb_read_total));
                nb_read_total += nb_read;
                buf += nb_read;
        }
        SDL_RWclose(rw);
        if (nb_read_total != res_size) {
                free(res);
                return NULL;
        }

        res[nb_read_total] = '\0';
        return res;
}

int file_write(const char *filename, char *string) {
	SDL_RWops *rw = SDL_RWFromFile(filename, "w");
	if (rw == NULL) return -1;
	size_t len = SDL_strlen(string);
	if (SDL_RWwrite(rw, string, 1, len) != len) {
		return -2; //printf("Couldn't fully write string\n");
	}
	SDL_RWclose(rw);
	return len;
}
