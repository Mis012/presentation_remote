LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := main

SDL_PATH := ../SDL
SOUP_INCLUDES := $(LOCAL_PATH)/../include/ $(LOCAL_PATH)/../include/glib-2.0/ $(LOCAL_PATH)/../lib/glib-2.0/include/ $(LOCAL_PATH)/../include/libsoup-2.4

LOCAL_C_INCLUDES := $(LOCAL_PATH)/$(SDL_PATH)/include $(SOUP_INCLUDES)

# Add your application source files here...
LOCAL_SRC_FILES := $(SDL_PATH)/src/main/android/SDL_android_main.c \
	main.c copy_asset.c kiss_widgets.c kiss_posix.c kiss_general.c \
	kiss_draw.c nanovg.c

LOCAL_SHARED_LIBRARIES := SDL2 SDL2_ttf SDL2_image libsoup-2.4

LOCAL_LDLIBS := -lGLESv1_CM -lGLESv2 -llog

include $(BUILD_SHARED_LIBRARY)
