/*
  kiss_sdl widget toolkit
  Copyright (c) 2016 Tarvo Korrovits <tkorrovi@mail.com>

  This software is provided 'as-is', without any express or implied
  warranty. In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would
     be appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not
     be misrepresented as being the original software.
  3. This notice may not be removed or altered from any source
     distribution.

  kiss_sdl version 2.0.0
*/

#include <string.h>

#include <android/log.h>

#include <libsoup/soup.h>

#include "kiss_sdl.h"

#include <GLES2/gl2.h>
#include "SDL.h"

#include "nanovg.h"
#include "nanovg_gl.h"
#include "nanovg_gl_utils.h"

extern void g_io_module_gnutls_load_static (void);

char* file_read(const char *filename);
int file_write(const char *filename, char *string);

SoupSession *session;
kiss_entry url_entry = {0};
kiss_entry password_entry = {0};

NVGcontext *vg;
kiss_array objects;

void control_eventloop() {
	SDL_Event e;

	int slideNum = -1; //will be 0 on first tap
	int update = 0;
	int quit = 0;
	
	while (!quit) {

		SDL_Delay(10);
		while (SDL_PollEvent(&e)) {
			if (e.type == SDL_QUIT) { quit = 1; }
			
			else if (e.type == SDL_MOUSEBUTTONUP) {
				slideNum++;
				update = 1;

			} else if (e.type == SDL_KEYDOWN && e.key.keysym.scancode == SDL_SCANCODE_AC_BACK) {
				slideNum--;
				update = 1;
			}
		}
		
		if (update != 1) {
			continue;
		}
		
		SoupMessage *msg = soup_message_new ("POST", url_entry.text);
		if(msg == NULL) {
			__android_log_print(ANDROID_LOG_ERROR, "PresentationRemote-control_eventloop", "error: soup_message_new failed");
			SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,
		                 "Couldn't connect",
		                 "soup_message_new failed.\nMost likely you messed up the protocol (http[s]://)",
		                 NULL);
			return; // TODO should print error on screen
		}
		char msg_body[22 + 3]; //22 = strlen("action=setSlide&slide="); 3 -> max 999 slides
		snprintf(msg_body, 22 + 3, "action=setSlide&slide=%d", slideNum);
		soup_message_set_request (msg, "application/x-www-form-urlencoded", SOUP_MEMORY_COPY, msg_body, strlen (msg_body));
		
		guint status = soup_session_send_message (session, msg);
		if(msg->response_body->data == NULL) {
			__android_log_print(ANDROID_LOG_ERROR, "PresentationRemote-control_eventloop", "error: libsoup response is NULL");
			__android_log_print(ANDROID_LOG_ERROR, "PresentationRemote-control_eventloop", "status: %d", status);
			__android_log_print(ANDROID_LOG_ERROR, "PresentationRemote-control_eventloop", "reason phrase: %s", msg->reason_phrase);
			SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,
		                 "Couldn't connect",
		                 "msg->response_body->data is NULL.\nMost likely the url you entered is incorrect, or you don't have internet connection.",
		                 NULL);
			return; // TODO should print error on screen
		}
		nvgBeginFrame(vg, kiss_screen_width, kiss_screen_height, kiss_screen_scalefactor);

		kiss_draw_text(vg, 20, 100, (char *)msg->response_body->data, FONT_SIZE, "sans-light", nvgRGBA(255,255,255,160), NVG_ALIGN_LEFT|NVG_ALIGN_MIDDLE);

		nvgEndFrame(vg);
		SDL_GL_SwapWindow(kiss_array_data(&objects, 0));
	}
}

static void button_ok_event(kiss_button *button_ok, SDL_Event *e,
	int *quit, int *draw)
{
	if (kiss_button_event(button_ok, e, draw)) {
	
		SoupMessage *msg = soup_message_new ("POST", url_entry.text);
		if(msg == NULL) {
			__android_log_print(ANDROID_LOG_ERROR, "PresentationRemote-button_ok_event", "error: soup_message_new failed");
			SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,
                         "Couldn't connect",
                         "soup_message_new failed.\nMost likely you messed up the protocol (http[s]://)",
                         NULL);
			return; // TODO should print error on screen
		}
		char msg_body[17 + KISS_MAX_LENGTH] = "action=auth&pass="; //17 = strlen("action=auth&pass=")
		strcat(msg_body, password_entry.text);
		soup_message_set_request (msg, "application/x-www-form-urlencoded", SOUP_MEMORY_COPY, msg_body, strlen (msg_body));
		
		guint status = soup_session_send_message (session, msg);
		if(msg->response_body->data == NULL) {
			__android_log_print(ANDROID_LOG_ERROR, "PresentationRemote-button_ok_event", "error: libsoup response is NULL");
			__android_log_print(ANDROID_LOG_ERROR, "PresentationRemote-button_ok_event", "status: %d", status);
			__android_log_print(ANDROID_LOG_ERROR, "PresentationRemote-button_ok_event", "reason phrase: %s", msg->reason_phrase);
			SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,
                         "Couldn't connect",
                         "msg->response_body->data is NULL.\nMost likely the url you entered is incorrect, or you don't have internet connection.",
                         NULL);
			return; // TODO should print error on screen
		}
		
		if(strstr(msg->response_body->data, "IN")) {
			control_eventloop();
		}
		
		__android_log_print(ANDROID_LOG_ERROR, "PresentationRemote-button_ok_event", "reply: %s", msg->response_body->data);//DEBUG
		
		g_object_unref(msg);
	}
}

int main(int argc, char **argv)
{
	g_io_module_gnutls_load_static(); // https support for libsoup
	
	SDL_Event e;
	kiss_window window;
	kiss_button button_ok = {0};

	int entry_width;
	int first, draw, quit, i;
	
	session = soup_session_new();
	SoupCookieJar *cookie_jar = soup_cookie_jar_new();
	soup_session_add_feature (session, (SoupSessionFeature *)cookie_jar); //for PHP session id
	
	const char *data_path = SDL_AndroidGetInternalStoragePath();
	char *ca_certificates_path = malloc(strlen(data_path)*sizeof(char) + strlen("ca-certificates.crt")*sizeof(char) + 1);
	*(ca_certificates_path) = '\0';
	strcat(ca_certificates_path, data_path);
	strcat(ca_certificates_path, "/ca-certificates.crt");
	
	if(access(ca_certificates_path, F_OK) == -1) {
		char *ca_certificates = file_read("ca-certificates.crt");
		file_write(ca_certificates_path, ca_certificates);
		free(ca_certificates);
	}

	g_object_set (G_OBJECT (session),
	"ssl-ca-file", ca_certificates_path,
	NULL); //for https support
	free(ca_certificates_path);
	
	vg = kiss_init("presentation remote", &objects, 640, 420);
	if (!vg) { 
		__android_log_print(ANDROID_LOG_ERROR, "PresentationRemote-main", "error: kiss_init failed");
		return 1; 
	}

	entry_width = kiss_screen_width - 2 * 20;

	/* Arrange the widgets nicely relative to each other */
	kiss_window_new(&window, NULL, 1, 0, 0, kiss_screen_width, kiss_screen_height);

	kiss_entry_new(&url_entry, &window, "", 20, 20, entry_width);
	kiss_entry_new(&password_entry, &window, "", 20, url_entry.rect.y + ENTRY_HEIGHT + 20 , entry_width);

	kiss_button_new(&button_ok, &window, "OK", 20, password_entry.rect.y + ENTRY_HEIGHT + 20);

	/* Do that, and all widgets associated with the window will show */
	window.visible = 1;

	quit = 0;
	draw = 1;
	first = 0;
	
	while (!quit) {

		/* Some code may be written here */

		SDL_Delay(10);
		while (SDL_PollEvent(&e)) {
			if (e.type == SDL_QUIT) quit = 1;

			kiss_window_event(&window, &e, &draw);
			kiss_entry_event(&url_entry, &e, &draw, vg); //TODO maybe use custom function?
			kiss_entry_event(&password_entry, &e, &draw, vg); //TODO maybe use custom function?

			button_ok_event(&button_ok, &e, &quit, &draw);
		}

		if (!draw) continue;

		nvgBeginFrame(vg, kiss_screen_width, kiss_screen_height, kiss_screen_scalefactor);

		kiss_window_draw(&window, vg);
		kiss_button_draw(&button_ok, vg);
		kiss_entry_draw(&url_entry, vg);
		kiss_entry_draw(&password_entry, vg);

		nvgEndFrame(vg);
		SDL_GL_SwapWindow(kiss_array_data(&objects, 0));
		draw = 0;
	}
	kiss_clean(&objects);

	return 0;
}

