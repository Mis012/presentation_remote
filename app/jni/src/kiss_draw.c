/*
  kiss_sdl widget toolkit
  Copyright (c) 2016 Tarvo Korrovits <tkorrovi@mail.com>

  This software is provided 'as-is', without any express or implied
  warranty. In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would
     be appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not
     be misrepresented as being the original software.
  3. This notice may not be removed or altered from any source
     distribution.

  kiss_sdl version 2.0.0
*/

/* edits for use with nanovg © 2018 Mis012 */
#include <android/log.h>

#include "kiss_sdl.h"

#include <GLES2/gl2.h>
#include "SDL.h"
#define NANOVG_GLES2_IMPLEMENTATION
#include "nanovg.h"
#include "nanovg_gl.h"
#include "nanovg_gl_utils.h"

kiss_font kiss_textfont, kiss_buttonfont;
kiss_image kiss_normal, kiss_prelight, kiss_active, kiss_bar,
	kiss_up, kiss_down, kiss_left, kiss_right, kiss_vslider,
	kiss_hslider, kiss_selected, kiss_unselected, kiss_combo;
int kiss_screen_width, kiss_screen_height;
int kiss_screen_scalefactor;
int kiss_textfont_size = 15;
int kiss_buttonfont_size = 12;
int kiss_click_interval = 140;
int kiss_progress_interval = 50;
int kiss_slider_padding = 2;
int kiss_edge = 2;
int kiss_border = 6;
double kiss_spacing = 0.5;
SDL_Color kiss_white = {255, 255, 255, 255};
SDL_Color kiss_black = {0, 0, 0, 255};
SDL_Color kiss_green = {0, 150, 0, 255};
SDL_Color kiss_blue = {0, 0, 255, 255};
SDL_Color kiss_lightblue = {200, 225, 255, 255};

unsigned int kiss_getticks(void)
{
	return SDL_GetTicks();
}

float kiss_textwidth(NVGcontext *vg, const char *font, float font_size, char *text1, char *text2) {
	float tw;
	
	nvgFontSize(vg, font_size);
	nvgFontFace(vg, font);
	tw = nvgTextBounds(vg, 0,0, text1, NULL, NULL);
	
	if (text2 != NULL) {
		tw += nvgTextBounds(vg, 0,0, text2, NULL, NULL);
	}
	
	return tw;
}

int kiss_create_font_from_file(NVGcontext* vg, const char* name, const char* filename) {
	SDL_RWops *rw = SDL_RWFromFile(filename, "rb");
	if (rw == NULL){ 
		__android_log_print(ANDROID_LOG_ERROR, "SDL-my-main", "error: file not found: %s", filename);
		return -1; 
	}
			 
	Sint64 res_size = SDL_RWsize(rw);
	char* res = (char*)malloc(res_size + 1);

	Sint64 nb_read_total = 0, nb_read = 1;
	char* buf = res;
	while (nb_read_total < res_size && nb_read != 0) {
		nb_read = SDL_RWread(rw, buf, 1, (res_size - nb_read_total));
		nb_read_total += nb_read;
		buf += nb_read;
	}
	SDL_RWclose(rw);
	if (nb_read_total != res_size) {
		free(res);
		__android_log_print(ANDROID_LOG_ERROR, "SDL-my-main", "error: read more than size of file");
		return -1;
	}
			 
	res[nb_read_total] = '\0';
	
	return nvgCreateFontMem(vg, name, (unsigned char*)res, res_size, 1);
}

void kiss_draw_text(NVGcontext* vg, int x, int y, char *text, float font_size, const char *font, NVGcolor font_color, int align) {

	nvgFontSize(vg, font_size);
	nvgFontFace(vg, font);
	nvgTextAlign(vg, align);
	nvgFillColor(vg, nvgRGBA(0,0,0,160));
	nvgText(vg, x, y-1, text, NULL);
	nvgFillColor(vg, font_color);
	nvgText(vg, x, y, text, NULL);
}

NVGcontext* kiss_init(char* title, kiss_array *a, int w, int h) {

	SDL_Window *window;
	//SDL_Renderer *renderer;
	SDL_GLContext context;
	SDL_Rect srect;
	int r;
	NVGcontext *vg = NULL;

	r = 0;
	
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) { //if it doesn't work, try and log a helpful error
		__android_log_print(ANDROID_LOG_ERROR, "SDL-kiss_init", "error: SDL_Init failed: %s", SDL_GetError());
		return NULL;
	}
	
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
	
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
	
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 8);
	
	SDL_GetDisplayBounds(0, &srect);
	if (!a || w > srect.w || h > srect.h) {
		__android_log_print(ANDROID_LOG_ERROR, "SDL-kiss_init", "error: SDL_GetDisplayBounds: %s", SDL_GetError());
		SDL_Quit();
		return NULL;
	}
	
	kiss_screen_width = w;
	kiss_screen_height = h;
	IMG_Init(IMG_INIT_PNG);
	TTF_Init();
	kiss_array_new(a);
	window = SDL_CreateWindow(title, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, w, h, SDL_WINDOW_OPENGL | SDL_WINDOW_FULLSCREEN_DESKTOP | SDL_WINDOW_SHOWN | SDL_WINDOW_ALLOW_HIGHDPI);
	if (window) {
		kiss_array_append(a, WINDOW_TYPE, window);
		SDL_GL_GetDrawableSize (window, &kiss_screen_width,
			&kiss_screen_height);
		kiss_screen_scalefactor = kiss_screen_width / w;
	} else {
		__android_log_print(ANDROID_LOG_ERROR, "SDL-kiss_init", "error: SDL_CreateWindow failed: %s", SDL_GetError());
	}
	
	context = SDL_GL_CreateContext(window);
	SDL_GL_MakeCurrent(window, context);
	
	vg = nvgCreateGLES2(NVG_ANTIALIAS | NVG_STENCIL_STROKES | NVG_DEBUG);
	if (vg == NULL) {
		__android_log_print(ANDROID_LOG_ERROR, "SDL-kiss_init", "error: nanovg init failed");
		return NULL;
	}
	
	kiss_create_font_from_file(vg, "sans-light", "Roboto-Light.ttf");
	
	return vg;	
}

int kiss_clean(kiss_array *a) {

	int i;

	if (!a) return -1;
	if (a->length)
		for (i = a->length - 1; i >= 0; i--) {
			if (kiss_array_id(a, i) == FONT_TYPE)
				TTF_CloseFont((TTF_Font *)
					kiss_array_data(a, i));
			else if (kiss_array_id(a, i) == TEXTURE_TYPE)
				SDL_DestroyTexture((SDL_Texture *)
					kiss_array_data(a, i));
			else if (kiss_array_id(a, i) == RENDERER_TYPE)
				SDL_DestroyRenderer((SDL_Renderer *)
					kiss_array_data(a, i));
			else if (kiss_array_id(a, i) == WINDOW_TYPE)
				SDL_DestroyWindow((SDL_Window *)
					kiss_array_data(a, i));
			else if (kiss_array_id(a, i) == ARRAY_TYPE)
				kiss_array_free((kiss_array *)
					kiss_array_data(a, i));
			else
				free (a->data[i]);
		}
	a->length = 0;
	kiss_array_free(a);
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
	return 0;
}

